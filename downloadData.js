import axiod from 'https://deno.land/x/axiod@0.24/mod.ts';
import pathExists from './lib/pathExists.js';

const
    client = axiod.create({ baseURL: 'https://api.github.com/repos/go-gitea/gitea' }),
    data = [];

{
    let chunk, page = 1;
    do {
        ({
            data: chunk
        } = await client.get('releases', { params: { 'per_page': 100, 'page': page++ } }));
        data.push(...chunk);
    }
    while(chunk && chunk.length);
}

await Deno.writeTextFile('./data/index.json', JSON.stringify(data, null, 4));

for(let i = 0; i < data.length; i++){
    const
        { [i]: { id }, length } = data,
        filePath = `./data/${id}.json`;
    if(!await pathExists(filePath))
        await Deno.writeTextFile(
            filePath,
            JSON.stringify(
                (await client.get(
                    `releases/${id}`,
                    {
                        headers: {
                            'authorization': `token ${Deno.env.get('TOKEN')}`
                        }
                    }
                )).data,
                null,
                4
            )
        );
    console.log(`${i+1}/${length} (${((i+1) / length * 100).toFixed(2)}%)`);
}