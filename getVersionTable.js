import AsciiTable from 'https://dev.jspm.io/npm:ascii-table@0.0.9';
import keywordRegexp from './lib/keywordRegexp.js';
import * as semver from 'https://deno.land/x/semver@v1.4.0/mod.ts';

const
    index = JSON.parse(await Deno.readTextFile('./data/index.json')),
    data = await Promise.all(index.map(async ({ id }) => JSON.parse(await Deno.readTextFile(`./data/${id}.json`)))),
    items = data.filter(item => !item['tag_name'].includes('-')).sort((a, b) => a.id - b.id),
    table = new AsciiTable().setHeading('ID', 'Actual version', 'Expected version', 'Actual increment', 'Expected increment');

{
    let version;
    for(let i = 0; i < items.length; i++){
        const {
            id,
            'tag_name': tagName,
            body
        } = items[i];
        if(body){
            const keywords = [...body.matchAll(keywordRegexp)].map(([, item]) => item);
            if(keywords.length){
                const actualIncrement = i > 0 ? semver.diff(items[i-1]['tag_name'].slice(1), tagName.slice(1)) : undefined;

                let increment = 'patch';

                if([
                    'FEATURE',
                    'ENHANCEMENT',
                    'TRANSLATION',
                    'FEATURES',
                    'ENHANCEMENTS',
                    'API'
                ].some(item => keywords.includes(item)))
                    increment = 'minor';

                if([
                    'BREAKING'
                ].some(item => keywords.includes(item)))
                    increment = 'major';

                version = version ? semver.inc(version, increment) : '1.0.0';

                const displayVersion = `v${version}`;

                table.addRow(
                    id,
                    tagName,
                    tagName === displayVersion ? '(same)' : displayVersion,
                    actualIncrement,
                    actualIncrement === increment ? '(same)' : increment
                );
            }
            else
            table.addRow(id, tagName, 'N/A', 'N/A', 'N/A');
        }
        else
        table.addRow(id, tagName, 'N/A', 'N/A', 'N/A');
    }
    console.log(table.toString());
}