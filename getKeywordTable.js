import AsciiTable from 'https://dev.jspm.io/npm:ascii-table@0.0.9';
import keywordRegexp from './lib/keywordRegexp.js';

const
    index = JSON.parse(await Deno.readTextFile('./data/index.json')),
    data = await Promise.all(index.map(async ({ id }) => JSON.parse(await Deno.readTextFile(`./data/${id}.json`)))),
    table = new AsciiTable().setHeading('Keyword', 'Version count'),
    keyword = {};

for(const { body } of data){
    if(body)
        [...body.matchAll(keywordRegexp)].map(([, item]) => item).forEach(item => keyword[item] = (keyword[item] || 0) + 1);
}

Object.entries(keyword).sort(([, a], [, b]) => b - a).map(item => table.addRow(...item));

console.log(table.toString());