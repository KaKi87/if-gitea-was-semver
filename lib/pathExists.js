export default async path => {
    try {
        await Deno.stat(path);
        return true;
    }
    catch(error){
        if(error.constructor.name === 'NotFound')
            return false;
        else
            throw error;
    }
};